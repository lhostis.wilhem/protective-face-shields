## 3- Solution en bandeau flexible avec séparateur – 1 lanière  de 70cm

Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hopital d'Ixelles, et légèrement adaptée grâce aux retours de l’hôpital CHU Saint-Pierre à Bruxelles. Elle est très rapide à produire et moins cassante que la version rigide.

![](images/flexible-70cm-general.jpg)


#### Matériel

* Matériel
  * Une feuille de PVC A4 transparente.
  * Une feuille de Priplak® (polypropylene), d’épaisseur 1mm, de 70 cm de longueur minimum.
* Outils :
  * Découpeuse laser
  * Cutter
* Temps de production : 2 min/pièce


#### Processus de fabrication

##### Etape 1 – Découpe de la structure (« bandelette » ci-dessous)
Téléchargez le modèle ci-dessous (.SLDPRT ou .DXF) et effectuez la découpe.
* [Bandelette.SLDPRT](Bandeau-masque-anti-projection-700.SLDPRT)
* [Bandelette.dxf](Bandeau-masque-anti-projection-700.dxf)

![](images/flexible-70cm-bands.jpg)


##### Etape 2 - Découpez la feuille A4 transparente

Découpez dans la feuille transparente, 6 entailles de 22mm de long situées comme sur le plan ci-dessous (en mm). Il a été constaté qu’une entaille ajustée (de dimensions égales à la largeur des bandes) est nécessaire pour un meilleur maintien de la feuille transparente.

![](images/flexible-70cm-cuts.jpg)

Pour simplifier les découpes et l’assemblage, une version avec 4 entailles est également possible pour un maintien suffisant du masque :

![](images/flexible-70cm-cuts2.jpg)

##### Etape 3 – Assembler

L’assemblage se fait à l’hôpital, mais est décrit pour information :

1. Insérer la bandelette dans les entailles sur toute la longueur de la feuille

![](images/flexible-70cm-assembly1.png)


2. Retirez la partie centrale de la bandelette des 2 entailles centrales de la feuille transparente.

![](images/flexible-70cm-assembly2.png)

3. Insérez la petite lamelle dans l’encoche afin d’ajuster la distance entre le front et la feuille transparente.


![](images/flexible-70cm-assembly3.png)

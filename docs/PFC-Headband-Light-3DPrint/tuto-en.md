# Solution at the 3D printer

Inspired by the protective face mask created by [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), we have developed a lighter version that is faster to produce due to time constraints under [Creative Commons BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/).

![](../images/P1-summary.jpeg)

## Hardware

* Material :
  * a transparent A4 sheet
  * PETG or PLA coil
* Tools:
  * 3D printer
  * punch (5 mm or 6 mm holes)
* Production time: 30 min per mask

## Manufacturing process

### Step 1 - Printing the structure

Download the template below designed by Nicolas De Coster under [Creative Commons BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/) and print it in PETG or PLA (30 min).

![](images/P1-3Dprint.jpeg)

For 6 mm punches (standard) :

* [model 1x](files/anti_projection_6mmPin.stl)

For 5 mm punches :

* [1x model](files/anti_projection_5mmPin.stl)

### Step 2 - Hole the transparent A4 sheet of paper

Using a punch, make 3 holes at a distance of 4 cm from the long edge of the transparent sheet.

[For the tutorial, we used a colored sheet]

![](../PFC-Headband-Light-3DPrint/images/P1-feuille3trous.jpeg)


#### 2 side holes

Make 2 holes on both sides of the transparency.
The distance between the hole and the hole and the side of the sheet is 11mm, while it is of 137.5mm between the central hole and the holes on the side.
Be careful to align the edge of the sheet with the central mark of the punch to position the hole (see photo below).

![](../PFC-Headband-Light-3DPrint/images/P1-trou1-2.jpeg)



#### the center hole

Fold the sheet in half and perforate in the centre of the fold at the level of the 2 other holes.

![](../PFC-Headband-Light-3DPrint/images/P1-trou_central.jpeg)


#### Drill method

It is also possible to make holes on a large number of sheets using a drill string.
With a 4.2mm drill bit, drill bundles of 100 still wrapped sheets, penetrating little by little to avoid heating of the bit and chips (causing internal welds).
Follow the template made previously or according to the following dimensions:


![](../PFC-Headband-Light-3DPrint/images/Gabarit_3_Trous.jpg)

An illustration of the perforation of a 100-sheet pack:

![](../PFC-Headband-Light-3DPrint/images/Perfo_100Feuilles.jpg)


### Step 3 - Elastic

Assembling the elastic band

![](../PFC-Headband-Light-3DPrint/images/P1-elastique.jpeg)

### Step 4 - Assemble

the assembly is done at the hospital.

## To go further and faster ##

It is also possible to print multiple stacked templates. This allows the printers to run for very long hours.

To see how to do this, go on our [Gitlab repository](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/tree/master/PFC-Headband-Light-3DPrint)

![](../PFC-Headband-Light-3DPrint/images/P1-3Dprint-stacked.jpeg)

## Delivery


![](../PFC-Headband-Light-3DPrint/images/P1-delivery.jpeg)

## Authors

Tutorial written by Denis Terwagne, (Frugal LAB & Fablab ULB, Université Libre de Bruxelles, Belgium) and Nicolas De Coster (IRM & Fablab ULB).  

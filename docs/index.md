# Protective Face Shields

[For English click here](index-en.md)

### Fabrication de masques anti projections pour les hopitaux dans les fablabs

Une équipe **pluridisciplinaire** du [Fablab ULB](http://fablab-ulb.be/) (Belgique) a développé des masques anti-projections en collaboration et à destination du CHU Saint-Pierre à Bruxelles. Nous sommes maintenant contactés par plusieurs hôpitaux de Belgique et de l'étranger. Initialement épaulés par le Fab-C à Charleroi et le fablab YourLab à Andenne, nous avons maintenant été rejoints par d'autres fablabs, associations et entreprises listées en fin de page.

Tous ensemble, au 30 mars 2020, nous avons produit plus de 5000 masques pour la Belgique.

Vous trouverez ci-dessous plusieurs tutoriels pour reproduire ces masques. Les 4 modèles sur ce site ont été validés par le CHU Saint-Pierre de Bruxelles.

![](./images/AllMasks.jpg)

## Infos pratiques

### Vous avez besoin de masques ?

Pour la région de **Charleroi Métropole dont Chimay**, merci de prendre contact avec Delphine Dauby ([Fab-C](https://www.fablab-charleroi.be/)) à l’adresse [delphine.dauby@ulb.ac.be](mailto:delphine.dauby@ulb.ac.be) (0479/209347 -  [facebook](https://www.facebook.com/notes/fab-c-fablab-charleroi-m%C3%A9tropole/des-visi%C3%A8res-pour-le-personnel-soignant-m%C3%A9dical-social-etc/2544439042483757/))

Pour la région de **Mons et alentours**, merci de prendre contact avec [martin.waroux@fablabmons.be](mailto:martin.waroux@fablabmons.be) ([FabLabMons](https://fablabmons.be/)) à l’adresse martin.waroux@fablabmons.be (0499/253305)

Pour la région de **Tournai - Wallonie Picarde**, merci de prendre contact avec François Bouton ([Fablab WAPI](https://www.fablabwapi.be/)) à l’adresse [francois.bouton@wapshub.be](mailto:francois.bouton@wapshub.be) (0477/134010)

Pour la région d'**Andenne**, merci de prendre contact avec Shirley Lefebvre ([YourLab](https://yourlab.be/)) à l’adresse [shirley@yourlab.be](mailto:shirley@yourlab.be) (0474/792535)

Pour **les autres régions dont Bruxelles**, merci de prendre contact avec Cécile Sztalberg de la [Fondation Michel Crémer](https://michelcremerfoundation.eu/) à l’adresse [covid@michelcremerfoundation.eu](mailto:covid@michelcremerfoundation.eu) qui soutient l'opération du  [Fablab ULB](http://fablab-ulb.be/).

### Vous désirez nous aider ?

##### Vous avez accès à des imprimantes 3D ou à des découpeuses lasers pour fabriquer des masques supplémentaires


1. Prenez contact avec le FabLab le plus proche de chez vous  :
   * Pour la région de **Charleroi Métropole dont Chimay** :  
 [Fab-C](https://www.fablab-charleroi.be/)  ([delphine.dauby@ulb.ac.be](mailto:delphine.dauby@ulb.ac.be) - 0479/209347 - [facebook](https://www.facebook.com/notes/fab-c-fablab-charleroi-m%C3%A9tropole/des-visi%C3%A8res-pour-le-personnel-soignant-m%C3%A9dical-social-etc/2544439042483757/))
   * Pour la région de **Mons et alentours** :  
 [FabLabMons](https://fablabmons.be/) ([martin.waroux@fablabmons.be](mailto:martin.waroux@fablabmons.be) - 0499/253305)
   * Pour la région de **Tournai - Wallonie Picarde** :  
 [Fablab WAPI](https://www.fablabwapi.be/) ([francois.bouton@wapshub.be](mailto:francois.bouton@wapshub.be) - 0477/134010)
   * Pour la région de **Namur** :  
[Trakk - Fablab](https://www.trakk.be/) ([fablab@trakk.be](mailto:fablab@trakk.be))
   * Pour la région d'**Andenne** :  
[YourLab](https://yourlab.be/) ([shirley@yourlab.be](mailto:shirley@yourlab.be) - 0474/792535)
   * Pour **les autres régions dont Bruxelles** :  
[Fablab ULB](http://fablab-ulb.be/) ([contact@fablab-ulb.be](mailto:contact@fablab-ulb.be) - [facebook](https://www.facebook.com/fablabULB/))

2. Fabriquez les masques en suivant les tutoriels ci-dessous (ces 4 modèles ont été approuvés par le CHU Saint-Pierre de Bruxelles).

3. Les Fablabs s'occuperont  de redistribuer les masques produits vers les hôpitaux et le personnel de 1ère ligne qui en ont besoin.

##### Le Fablab ULB a besoin de matériel !

Vous pouvez nous aider ? Déposez le matériel directement au Fablab ULB. Sont demandés :

* feuilles de plastic A4 de PCV de 300mx210mmx0.2mm (slides A4)
* enveloppes A4 à soufflet



## Modèles de masques et tutoriels

4 modèles ont été validés par les médecins hygiénistes et la direction de l’hôpital CHU Saint-Pierre à Bruxelles avec une première commande de 1500 unités à produire. Nous les partageons ci-dessous.

### 1 - à l'imprimante 3D

Inspiré du masque de protection créé par [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), nous avons développé une version plus légère et plus rapide à produire vu les contraintes de temps.

![](./images/P1-summary.jpeg)

#### Matériel et outils

* Matériel :
  * une feuille A4 transparente
  * bobine de PETG ou de PLA
* Outils :
  * imprimante 3D
  * perforatrice (trous de 5 mm ou 6 mm)
* Temps de production : 30 min par masque

#### Tutoriel

[Tutoriel ici](./PFC-Headband-Light-3DPrint/tuto-fr.md)

### 2 - Solution à la découpeuse laser

Nous avons fait valider [ce modèle de chez Thingiverse conçu par LadyLibertyHK](http://thingiverse.com/thing:4159366) par l'hôpital CHU Saint-Pierre à Bruxelles.

![](./images/P2-summary.jpeg)


#### Matériel et outils

* Matériel
  * plaque de plexiglas en 3 mm
  * feuille A4 transparente
* Outils :
  * découpeuse laser
  * une foreuse + mêche 3.2 mm
* Temps de production : 1 minute par masque

#### Tutoriel

[Tutoriel ici](./PFC-Cap-LaserCut/tuto-fr.md)

### 3 - Solution en bandeau flexible à la découpeuse laser

Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hôpital d'Ixelles, et légèrement adaptée grâce aux retours de l’hôpital CHU Saint-Pierre à Bruxelles. Elle est très rapide à produire et moins cassante que la version rigide.

![](./images/headband-flexible-general.png)

#### Matériel

* Matériel
  * Une feuille de PVC A4 transparente.
  * Une feuille de Priplak® (polypropylene), d’épaisseur 1mm, de 70 cm de longueur minimum.
* Outils :
  * Découpeuse laser
  * Cutter
* Temps de production : 2 min/pièce

#### Tutoriel

Le tutoriel complet ansi que les fichiers pour sa réalisation [sont disponibles ici](PFC-Headband-Flexible-LaserCut/tuto-fr.md).

### 3bis -  en bandeau flexible simple à la découpeuse laser

Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hopital d'Ixelles, et partagée sous une [licence Creative Commons BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/). Elle est très rapide à produire et moins cassante que la version rigide.

**Cette version est une alternative du bandeau flexible à la découpe laser.**

![](PFC-Headband-Flexible-LaserCut-Simple/images/general.png)

#### Matériel
*	Matériel :
	 * Une feuille de PVC A4 transparente.
   * Une feuille de Priplak® (polypropylene), d’épaisseur 1mm, de 70 cm de longueur minimum.
* Outils :
  * Découpeuse laser
  * Cutter
*	Temps de production : 1min30/pièce

#### Tutoriel

Le tutoriel complet ansi que les fichiers pour sa réalisation [sont disponibles ici](PFC-Headband-Flexible-LaserCut-Simple/tuto-fr.md).

### 3ter - en bandeau flexible au cutter

Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hopital d'Ixelles, et partagée sous une [licence Creative Commons BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/). Elle est très rapide à produire et ne nécessite aucune machine technologique de découpe.

Cette version est une alternative du [bandeau flexible à la découpe laser.

![](PFC-Headband-Flexible-Cutter/files/headband-flexible-cutter-general.jpg)

#### Matériel

* Matériel :
  * Une feuille de PVC A4 transparente.
  * Une feuille de Priplak® (polypropylene), d’épaisseur 1mm, de 70 cm de longueur minimum.
* Outils :
  * Cutter ou une paire de ciseaux
* Temps de production : 5min/pièce

#### Tutoriel

Le tutoriel complet [est disponibles ici](PFC-Headband-Flexible-Cutter/tuto-fr.md).



### 4 - en charlotte

Une simple solution issue du service de Stomatologie et chirurgie maxillo-faciale du CHU St-Pierre à Bruxelles et du Fablab ULB

![](./images/P4-Hygiene_cap.jpeg)

#### Matériel

* Matériel
  * Une charlotte
  * Une feuille de plastique A4 transparente.
* Outils :
  * Agrafeuse
* Temps de production : 1 min/pièce

#### Tutoriel

Le tutoriel complet ansi que les fichiers pour sa réalisation [sont disponibles ici](PFC-Hygiene-Cap/tuto-fr.md).

## Protocole de désinfection des masques

Un protocole de désinfection réalisé par le CHU Saint-Pierre de Bruxelles peut [être téléchargé ici](images/Cleaning-protocol-fr.pdf).

## Qui sommes nous ?

Le [Fablab ULB](http://fablab-ulb.be/) est une équipe pluridisciplinaire d'académiques, chercheurs, techniciens, étudiants de différentes Facultés (Sciences, Architecture, Droit, Ecole Polytechnique de Bruxelles) et aussi d'artistes, designers, makers et citoyens. Il est appuyé dans sa mission par différents laboratoires de recherche de l'ULB à savoir le Frugal LAB (Faculté des Sciences), le Juris LAB (Faculté de Droit) et le BEAMS (Ecole Polytechnique de Bruxelles).

## Avec la précieuse collaboration de

Ce projet particulier a été réalisé en collaboration avec le [Fab-C](https://www.fablab-charleroi.be) de Charleroi et le fablab [YourLab](https://yourlab.be/) d'Andenne et soutenu par la [fondation Michel Crémer](https://michelcremerfoundation.eu/).

Nous avons maintenant été rejoints par d'autres fablabs, associations et entreprises :

* [FabLabMons](https://fablabmons.be/)
* [Fablab WAPI](https://www.fablabwapi.be/)
* [Expérimentarium de physique de l'ULB (Faculté des Sciences)](http://www.experimentarium.be/)
* [La Scientothèque](https://lascientotheque.be/)
* [ISIB](https://www.he2b.be/campus-isib)
* [Bureau d'architecture A2RC](https://www.a2rc.be/)
* [Bureau d'architecture Jaspers-Eyers](https://www.jaspers-eyers.be/)
* [Elysta](https://elysta.be/)
* [City Fab 2 (Bruxelles)](https://www.citydev.brussels/fr/projets/cityfab-2)
* Melt+ []()
* et de nombreux particuliers (liste à venir)

## Ils parlent de nous

En ces temps de crises, les journalistes font parfois de malheureux raccourcis :  
Dans les articles ci-dessous, **entendez par "ingénieurs" -> une "équipe pluridisciplinaire de scientifiques, architectes, designers, ingénieurs, médecins et techniciens"**.  
La pluridisciplinarité est l'ADN du Fablab !

* [RTBF JT du 19h30, le 23 mars 2020](https://michelcremerfoundation.eu/wp-content/uploads/2020/03/Les-ing%C3%A9nieurs-ont-mis-au-point-des-surmasques.mp4)
* [Actus ULB, 25 mars 2020](https://actus.ulb.be/fr/actus/institution-et-engagements/des-visieres-pour-proteger-nos-soignants)
* [Journal La Libre Belgique, 26 mars 2020](https://www.lalibre.be/planete/sante/les-fablabs-developpent-un-surmasque-a-destination-du-personnel-medical-5e7b6b4e7b50a6162bbc5af3)
* [Présentation de l'opération des masques au réseau mondial des fablabs via la Fab Academy (10 premières minutes)](https://vimeo.com/402353963)

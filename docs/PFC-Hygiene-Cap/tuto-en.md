# Hygiene Cap Solution

A simple solution from the department of Stomatology and maxillofacial surgery of the CHU St-Pierre in Brussels and from the Fablab ULB ([Creative Commons license BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/en/)).

![](../images/P4-Hygiene_cap.jpeg)

#### Material

* Material
  * A Hygiene Cap
  * A sheet of transparent A4 plastic.
* Tools:
  * Stapler
* Production time: 1 min/piece

## Processus de fabrication

<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="../video_thumbnail.png">
    <source src="../video.mp4" type="video/mp4">
  </video>
</figure>

## Authors

Solution created by

* Service de Stomatologie et chirurgie maxillo-faciale du CHU St-Pierre à Bruxelles :  
Aurélien Termont, Cyril Bouland, Antoine Yanni, Nadia Dahdouh, Thibaut Buset, Cynthia Watteeuw, Rokneddine Javadian, Didier Dequanter, Xavier Vanden Eynden, Caroline Gossiaux, Edward Boutremans, Medin Rey Susanna, Isabelle Loeb
* Fablab ULB: Victor Lévy
